/**
 * Represents a cell in the cache.
 * Cell includes a valid flag, modified flag, tag and data.
 * Note that since we're only interested in misses and hits,
 * we do not store the data here.
 */
public class Cell {
	/**
	 * Whether or not data in the cell is valid.
	 */
	private boolean isValid = false;
	/**
	 * Whether or not data in the cell is modified after its set from disk.
	 */
	private boolean isModified = false;
	/**
	 * Tag of the block.
	 */
	private int tag;
	
	/**
	 * Represents a get reference to the block.
	 * @return true if data in the block is valid. false otherwise.
	 */
	public boolean get(){
		if(isValid){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the tag matches.
	 * @param tag
	 * @return true if cache matches, false otherwise.
	 */
	public boolean match(int tag){
		boolean result = false;
		if(this.isValid && this.tag == tag)
			result = true;
		
		return result;
	}
	
	/**
	 * Sets the tag.
	 * Used when a new block is moved to cell.
	 * @param tag
	 */
	public void set(int tag){
		this.isValid = true;
		this.tag = tag;
		this.isModified = false;
	}
	
	/**
	 * Writes data to the tag.
	 * Used when the data in the cache is modified.
	 * Different then set because this only modifies
	 * the cache.
	 */
	public void write(){
		this.isModified = true;
	}
	
	/**
	 * Resets the cell, i.e sets isValid to false.
	 */
	public void reset(){
		this.isValid = false;
		this.isModified = false;
	}

	/**
	 * @return the tag hold in the cell.
	 */
	public int getTag() {
		return this.tag;
	}
	
	/**
	 * @return true is the cell is modified during its lifetime.
	 */
	public boolean isModified() {
		return this.isModified;
	}
}
