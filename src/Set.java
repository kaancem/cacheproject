/**
 * Set of cells in the cache.
 * Number of cells depends on the associativity of the cache.
 * The eviction mechanism is random.
 */
public class Set {
	/**
	 * Associativity, represents number of cells in the set.
	 */
	private int associativity;
	/**
	 * Number of occupied cells.
	 */
	private int occupiedCount;
	/**
	 * Index of the set in the cache.
	 */
	private int index;
	/**
	 * Cells the set has.
	 */
	private Cell[] cells;
	private Logger logger = Logger.getInstance();
	
	/**
	 * Initializes the member variables.
	 * @param index is the index of this set in the cache.
	 * @param associativity number of cells in each cache entry.
	 */
	public Set(int index, int associativity){
		this.index = index;
		this.associativity = associativity;
		this.occupiedCount = 0;
		this.cells = new Cell[associativity];
		for(int i=0; i < associativity; i++){
			cells[i] = new Cell();
		}
	}
	
	/**
	 * Checks if get attempt is a miss or hit.
	 * If a miss occurs sets a cell with the tag.
	 * @param tag is the tag to look for.
	 * @return true on hit, false on miss.
	 */
	public boolean get(int tag){
		for(Cell cell: this.cells)
			if(cell.match(tag)){
				cell.get();
				return true;
			}
		// No matching cell found.
		moveToCache(tag);
		return false;
	}
	
	/**
	 * Tries to write to a block with given tag.
	 * @param tag of the memory block.
	 * @return false if block must be read from disk before writing to cache.
	 * true if block is only updated on cache.
	 */
	public boolean write(int tag){
		for(Cell cell: this.cells)
			if(cell.match(tag)){
				cell.write(); // modifies the data.
				return true; // found block in cache just write to cache.
			}
		
		moveToCache(tag); // not found block in cache write requires read from cache.
		return false;
	}
	
	/**
	 * Sets a cell in the entry with the tag.
	 * Used when an entire block must be read from disk and written to cache.
	 * If all cells are occupied, a random cell is picked and overwritten.
	 * Otherwise a random cell is picked.
	 * @param tag
	 */
	private void moveToCache(int tag){
		Cell cell = null;
		if(this.occupiedCount >= this.associativity){
			cell = this.cells[(int) (Math.random() * this.associativity)];
			if (cell.isModified()) // Write back if block is modified.
				logger.info("Evict block. Write back! Index: " + this.index + "\tTag: " + cell.getTag());
			else
				logger.info("Evict block. No write back. Index: " + this.index + "\tTag: " + cell.getTag());
		} else {
			cell = cells[this.occupiedCount];
			this.occupiedCount++;
		}
		
		cell.set(tag);
	}
	
	/**
	 * Resets all cells.
	 */
	public void reset(){
		for(Cell cell: this.cells){
			cell.reset();
		}
	}
}
