
/**
 * The cache for the simulation.
 * This implementation only concerns with the addresses
 * reference and whether or not they hit the cache.
 * Hence, no actual data is set or modified, only booleans
 * are returned representing hits and misses.
 */
public class Cache {
	/**
	 * Number of sets in the cache.
	 */
	private int setCount;
	/**
	 * Size of data blocks.
	 */
	private int blockSize;
	/**
	 * Number of miss occurrences on reads.
	 */
	private int missCount;
	/**
	 * Number of hit occurrences on reads.
	 */
	private int hitCount;
	/**
	 * Number of misses on writes.
	 */
	private int writeMissCount;
	/**
	 * Number of hits on writes.
	 */
	private int writeHitCount;
	/**
	 * Sets that cache has.
	 */
	private Set[] sets;
	private Logger logger = Logger.getInstance();
	
	
	/**
	 * Initializes the cache instance.
	 * @param entryCount is number of entries (cell collections) in the cache.
	 * @param cellCount is number of cells in each entry.
	 * @param blockSize is the number of bytes in each block. 
	 */
	public Cache(int cacheSize, int associativity, int blockSize){
		this.setCount = cacheSize / (blockSize * associativity);
		this.hitCount = 0;
		this.missCount = 0;
		this.writeHitCount = 0;
		this.writeMissCount = 0;
		this.blockSize = blockSize;
		this.sets = new Set[this.setCount];
		for(int i=0; i < this.setCount; i++){
			this.sets[i] = new Set(i, associativity);
		}
	}
	
	/**
	 * Attempts to access cache.
	 * After each resetFrequency calls cache is reset.
	 * @param address
	 * @return true if access is a hit, true otherwise.
	 */
	public boolean get(int address){
		int index = getIndex(address);
		int tag = getTag(address);

		boolean result = sets[index].get(tag);
		if(result){
			logger.info("Cache read hit! Address: " + address + "\tIndex: " + index + "\tTag: " + tag);
			hitCount++;
		}
		else{
			logger.info("Cache read miss! Address: " + address + "\tIndex: " + index + "\tTag: " + tag);
			missCount++;
		}

		return result;
	}
	
	/**
	 * Attempts to write a to an address.
	 * @param address
	 */
	public void write(int address){
		int index = getIndex(address);
		int tag = getTag(address);
		
		boolean result = this.sets[index].write(tag);
		if(result){
			logger.info("Cache write updated cache only! Address: " + address + "\tIndex: " + index + "\tTag: " + tag);
			this.writeHitCount++;
		} else {
			logger.info("Cache write miss, block read from disk! Address: " + address + "\tIndex: " + index + "\tTag: " + tag);
			this.writeMissCount++;
		}
	}
	/**
	 * Resets whole cache.
	 */
	public void reset(){
		for(Set entry: this.sets){
			entry.reset();
		}
	}
	
	/**
	 * @return number of misses on misses.
	 */
	public int getReadMissCount(){
		return this.missCount;
	}
	
	/**
	 * @return number of hits on hits.
	 */
	public int getReadHitCount(){
		return this.hitCount;
	}
	
	/**
	 * @return number of misses on writes.
	 */
	public int getWriteMissCount(){
		return this.writeMissCount;
	}
	
	/**
	 * @return number of hits on hits on writes.
	 */
	public int getWriteHitCount(){
		return this.writeHitCount;
	}
	
	/**
	 * Extracts index from the address.
	 * @param address
	 * @return index portion of the address.
	 */
	private int getIndex(int address){
		return (address / this.blockSize) % this.setCount;
	}
	
	/**
	 * Extracts tag from the address.
	 * @param address
	 * @return tag portion of the address.
	 */
	private int getTag(int address){
		return (address / (this.blockSize * this.setCount));
	}
}
